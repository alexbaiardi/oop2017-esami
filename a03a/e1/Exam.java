package a03a.e1;

public class Exam {
	
	private final int examId;
	private final String examName;
	private boolean started;
	
	public Exam(int examId, String examName) {
		this.examId = examId;
		this.examName = examName;
		this.started=false;
	}

	public int getExamId() {
		return examId;
	}

	public String getExamName() {
		return examName;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public boolean isStarted() {
		return started;
	}
	
	
	

}
