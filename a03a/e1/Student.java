package a03a.e1;

import java.util.LinkedList;
import java.util.List;

public class Student {
	
	private final int studentId;
	private final String name;
	private final List<Registration>  registrations;
	
	public Student(int studentId, String name) {
		registrations=new LinkedList<Registration>();
		this.studentId = studentId;
		this.name = name;
	}

	public int getStudentId() {
		return studentId;
	}

	public String getName() {
		return name;
	}

	public List<Registration> getRegistrations() {
		return registrations;
	}
	
	
	

}
