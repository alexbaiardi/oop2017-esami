package a03a.e1;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class ExamsManagementImpl implements ExamsManagement {

	LinkedList<Exam> exams;
	List<Student> students;
	
	
	
	public ExamsManagementImpl() {
		exams=new LinkedList<Exam>();
		students=new LinkedList<Student>();
	}

	@Override
	public void createStudent(int studentId, String name) {
		students.add(new Student(studentId, name));
	}

	@Override
	public void createExam(String examName, int incrementalId) {
		check(incrementalId>exams.getLast().getExamId());
		exams.add(new Exam(incrementalId, examName));
	}

	@Override
	public void registerStudent(String examName, int studentId) {
		Optional<Student> stud=getStudentFromId(studentId);
		Optional<Exam> ex=getExamFromName(examName);
		check(stud.isPresent());
		check(ex.isPresent());
		stud.get().getRegistrations().add(new Registration(studentId, ex.get().getExamId()));
	}

	@Override
	public void examStarted(String examName) {
		Optional<Exam> ex=getExamFromName(examName);
		check(ex.isPresent());
		check(exams.stream().filter(e->e.isStarted()).findFirst().isEmpty());
		ex.get().setStarted(true);
	}

	@Override
	public void registerEvaluation(int studentId, int evaluation) {
		Optional<Student> stud=getStudentFromId(studentId);
		check(stud.isPresent());
		stud.get().getRegistrations();
	}

	@Override
	public void examFinished() {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<Integer> examList(String examName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Integer> lastEvaluation(int studentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Integer> examStudentToEvaluation(String examName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Integer, Integer> examEvaluationToCount(String examName) {
		// TODO Auto-generated method stub
		return null;
	}

	private void check(boolean condition) {
		if(!condition) {
			throw new IllegalStateException();
		}
		
	}
	
	private Optional<Exam> getExamFromName(String examName){
		return exams.stream().filter(e->e.getExamName().equals(examName)).findFirst();
	}
	private Optional<Student> getStudentFromId(int id){
		return students.stream().filter(s->s.getStudentId()==id).findFirst();
	}
}
