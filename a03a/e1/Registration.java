package a03a.e1;

import java.util.Optional;

public class Registration {
	
	private final int studentId;
	private final int examId;
	private Optional<Integer> evaluation;
	
	public Registration(int studentId, int examId) {
		this.studentId = studentId;
		this.examId = examId;
		evaluation=Optional.empty();
	}

	public Optional<Integer> getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(Optional<Integer> evaluation) {
		this.evaluation = evaluation;
	}

	public int getStudentId() {
		return studentId;
	}

	public int getExamId() {
		return examId;
	}	

}
