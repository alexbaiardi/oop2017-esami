package a02b.e1;

import java.util.LinkedList;
import java.util.stream.Stream;

public class TransducerFactoryImpl implements TransducerFactory {

	@Override
	public <X> Transducer<X, String> makeConcatenator(int inputSize) {
		return new Transducer<X, String>() {
			private LinkedList<X> input=new LinkedList<X>();
			private LinkedList<String> output=new LinkedList<String>();
			private boolean inputOver=false;
			private boolean outputOver=false;

			@Override
			public void provideNextInput(X x) {
				input.add(x);
				if(input.size()==inputSize)
				{
					output.add(input.stream().map(X::toString).reduce((z,y)->z.toString()+y.toString()).get());
					input.clear();
				}
			}

			@Override
			public void inputIsOver() {
				check(!inputOver);
				inputOver=true;
				if (!input.isEmpty()) {
					output.add(input.stream().map(X::toString).reduce((z, y) -> z.toString() + y.toString()).get());
					input.clear();
				}
				outputOver=output.isEmpty();
			}

			@Override
			public boolean isNextOutputReady() {
				return !output.isEmpty();
			}

			@Override
			public String getOutputElement() {
				check(!output.isEmpty());
				String s= output.remove();
				outputOver=output.isEmpty();
				return s;
			}

			@Override
			public boolean isOutputOver() {
				return output.isEmpty() && input.isEmpty() && outputOver;
			}
			
			private void check(boolean condition) {
				if(!condition) {
					throw new IllegalStateException();
				}
			}
		};
	}

	@Override
	public Transducer<Integer, Integer> makePairSummer() {
		return new Transducer<>() {
			private LinkedList<Integer> input=new LinkedList<>();
			private LinkedList<Integer> output=new LinkedList<>();
			private boolean inputOver=false;
			private boolean outputOver=false;

			@Override
			public void provideNextInput(Integer x) {
				input.add(x);
				if(input.size()==2)
				{
					output.add(input.stream().reduce((z,y)->z+y).get());
					input.clear();
				}
			}

			@Override
			public void inputIsOver() {
				check(!inputOver);
				inputOver=true;
				if (!input.isEmpty()) {
					output.add(input.getFirst());
					input.clear();
				}
				outputOver=output.isEmpty();
			}

			@Override
			public boolean isNextOutputReady() {
				return !output.isEmpty();
			}

			@Override
			public Integer getOutputElement() {
				check(!output.isEmpty());
				Integer i= output.remove();
				outputOver=output.isEmpty();
				return i;
			}

			@Override
			public boolean isOutputOver() {
				return output.isEmpty() && input.isEmpty() && outputOver;
			}
			
			private void check(boolean condition) {
				if(!condition) {
					throw new IllegalStateException();
				}
			}
		};
	}

}
