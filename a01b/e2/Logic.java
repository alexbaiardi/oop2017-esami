package a01b.e2;

public interface Logic {
    
    boolean hit(int i);
    int getAttempts();
    int getValue(int i);

}
