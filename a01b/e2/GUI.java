package a01b.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.stream.*;

import javax.swing.*;

public class GUI extends JFrame{
	
    private final List<JButton> buttons;
    private final Logic logic;
	
	public GUI(int size){
	    buttons=new ArrayList<>();
	    logic=new LogicImpl(size);
	    this.setSize(size*70, 100);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.getContentPane().setLayout(new FlowLayout());
        
        ActionListener ac = (src)-> {
            JButton b =(JButton)src.getSource();
            if(logic.hit(buttons.indexOf(b))) {
                b.setEnabled(false);
                b.setText(String.valueOf(logic.getValue(buttons.indexOf(b))));
            }
            System.out.println("Attempts: "+logic.getAttempts());
        };
        
        for(int i=0;i<size;i++) {
            JButton bt = new JButton("*");
            bt.addActionListener(ac);
            this.getContentPane().add(bt);
            buttons.add(bt);
        }
        
        this.setVisible(true);
	}
	
}
