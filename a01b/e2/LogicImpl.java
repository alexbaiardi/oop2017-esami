package a01b.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class LogicImpl implements Logic {

    private static final int MAX=100;
    private int attempts;
    private final List<Integer> values;
    private final List<Integer> valuesHit;
    
    
    
    public LogicImpl(int size) {
        this.attempts = 0;
        Random r=new Random();
        this.values = new ArrayList<Integer>(Stream.generate(()->r.nextInt(MAX)).limit(size).collect(Collectors.toList()));
        this.valuesHit=new ArrayList<Integer>(Stream.generate(()->MAX).limit(size).collect(Collectors.toList()));
    }

    @Override
    public boolean hit(int i) {
        attempts++;
        if(values.get(i)==min(values)) {
            valuesHit.set(i,values.get(i));
            values.set(i,MAX);
            return true;
        }
        return false;
    }

    @Override
    public int getAttempts() {
        return attempts;
    }

    @Override
    public int getValue(int i) {
        return valuesHit.get(i);
    }
    
    private int min(List<Integer> l) {
        return values.stream().min((x,y)->x-y).get();
    }

}
