package a01b.e1;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class TraceFactoryImpl implements TraceFactory {

	@Override
	public <X> Trace<X> fromSuppliers(Supplier<Integer> sdeltaTime, Supplier<X> svalue, int size) {
		Iterator<Integer> timeIterator=Stream.iterate(0, i->i+sdeltaTime.get()).iterator();
		Stream<EventImpl<X>> s=Stream.generate(()->new EventImpl<X>(timeIterator.next(), svalue.get())).limit(size);
		return new Trace<X>() {
			
			Iterator<Event<X>> it=s.map(e->(Event<X>)e).iterator();
			
			@Override
			public Optional<Event<X>> nextEvent() {
				Event<X> next=null;
				if(it.hasNext()) {
					next=it.next();
				}
				return Optional.ofNullable(next);
			}

			@Override
			public Iterator<Event<X>> iterator() {
				return it;
			}

			@Override
			public void skipAfter(int time) {
				//Stream.iterate(it.next(), it.hasNext(),it.next());
			}

			@Override
			public Trace<X> combineWith(Trace<X> trace) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Trace<X> dropValues(X value) {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

	@Override
	public <X> Trace<X> constant(Supplier<Integer> sdeltaTime, X value, int size) {
		return fromSuppliers(sdeltaTime, ()->value, size);
	}

	@Override
	public <X> Trace<X> discrete(Supplier<X> svalue, int size) {
		return fromSuppliers(()->1,svalue, size);
	}

}
