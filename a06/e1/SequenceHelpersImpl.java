package a06.e1;

import java.util.Iterator;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

public class SequenceHelpersImpl implements SequenceHelpers {

    @Override
    public <X> Sequence<X> of(X x) {
        return ()->x;
    }

    @Override
    public <X> Sequence<X> cyclic(List<X> l) {
        Iterator<X> it=Stream.generate(()->l).flatMap(e->e.stream()).iterator();
        return ()->it.next();
    }

    @Override
    public Sequence<Integer> incrementing(int start, int increment) {
        Iterator<Integer> it=Stream.iterate(start, e->e+increment).iterator();
        return ()->it.next();
    }

    @Override
    public <X> Sequence<X> accumulating(Sequence<X> input, BinaryOperator<X> op) {
        Iterator<X> it=Stream.iterate(input.nextElement(), e->op.apply(e, input.nextElement())).iterator();
        return ()->it.next();
    }

    @Override
    public <X> Sequence<Pair<X, Integer>> zip(Sequence<X> input) {
        Iterator<Pair<X, Integer>> it=Stream.iterate(new Pair<X, Integer>(input.nextElement(),0),e->new Pair<>(input.nextElement(), e.getY()+1)).iterator();
        return ()->it.next();
    }

}
