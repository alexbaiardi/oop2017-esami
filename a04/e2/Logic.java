package a04.e2;

public interface Logic {
	
	public enum Result {
		STRAIGH,
		YAHTZEE,
		FOUR,
		FULL,
		THREE,
		NOTHING
	}
	
	void select(int i);
	void generate();
	Result getResult();
	
	int getValue(int i);
	
	
	

}
