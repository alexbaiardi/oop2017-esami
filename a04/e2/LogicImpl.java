package a04.e2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogicImpl implements Logic {
	
	private final List<Integer> values;
	private final Set<Integer> posToGenerate;
	
	public LogicImpl() {
		//values= Stream.generate(()->r.nextInt(6)+1).limit(5).collect(Collectors.toList());
		values=new ArrayList<Integer>(List.of(0,0,0,0,0));
		posToGenerate=Stream.iterate(0, i->i+1).limit(5).collect(Collectors.toSet());
		generate();
	}

	@Override
	public void select(int i) {
		posToGenerate.add(i);
	}

	@Override
	public void generate() {
		Random r= new Random();
		posToGenerate.forEach(e->values.set(e, r.nextInt(6)+1));
		posToGenerate.clear();
	}

	@Override
	public Result getResult() {
		/*
		if(distinctValue()==1) {
			return Result.YAHTZEE;
		}
		if(distinctValue()==2) {
			if(numCopy(values.get(0))==2||numCopy(values.get(0))==3) {		
				return Result.FULL;
			}
			return Result.FOUR;
		}
		if(distinctValue()==3)
		{
			
		}
		*/
		//System.out.println(values);
		for(int i=1;i<7;i++) {
			switch (numCopy(i)) {
			case 5:
				return Result.YAHTZEE;
			case 4:
				return Result.FOUR;
			case 3:
				if (distinctValue() == 2) {
					return Result.FULL;
				}
				return Result.THREE;
			default:
				break;
			}
		}

		if (distinctValue() == 5 && numCopy(1) + numCopy(6) == 1) {
			return Result.STRAIGH;
		}
		return Result.NOTHING;
	}
	
	private int numCopy(int i) {
		return (int) values.stream().filter(e->e==i).count();	
	}
	
	private int distinctValue() {
		return (int)values.stream().distinct().count();
	}

	@Override
	public int getValue(int i) {
		return values.get(i);
	}

}
