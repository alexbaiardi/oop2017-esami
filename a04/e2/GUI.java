package a04.e2;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;


public class GUI extends JFrame {
	private final Logic logic;
	private final List<JButton> buttons;

	public GUI() {
		logic=new LogicImpl();
		buttons= new ArrayList<>();
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());

		
		

		ActionListener ac = e -> {
			final JButton jb = (JButton) e.getSource();
			logic.select(buttons.indexOf(jb));
			jb.setEnabled(false);
		};
		for(int i=0;i<5;i++) {
			JButton jb = new JButton(String.valueOf(logic.getValue(i)));
			jb.addActionListener(ac);
			this.getContentPane().add(jb);
			buttons.add(i, jb);
		}
		JButton draw = new JButton("Draw");
		this.getContentPane().add(draw);
		ActionListener aDraw= e->{
			logic.generate();
			System.out.println(logic.getResult());
			drawButtons();
		};
		draw.addActionListener(aDraw);
		
		this.setVisible(true);
	}

	private void drawButtons() {
		buttons.forEach(b->{
			b.setEnabled(true);
			b.setText(String.valueOf(logic.getValue(buttons.indexOf(b))));
		});
	}
	
}

