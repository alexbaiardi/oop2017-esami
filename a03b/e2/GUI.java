package a03b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {
    
	private final Map<JButton,Pair<Integer,Integer>> buttons;
	private final Logic logic; 
	private int size;
     
    public GUI(int size) {
    	buttons=new HashMap<>();
    	logic=new LogicImpl(size);
    	this.size=size;
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(size*100, size*100);
        JPanel panel = new JPanel(new GridLayout(size,size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            logic.hit(buttons.get(bt).getX(), buttons.get(bt).getY());
            draw();
        };
        for (int i=0;i<size;i++){
        	for(int j=0;j<size;j++) {
        		final JButton jb = new JButton();
        		jb.addActionListener(al);
        		panel.add(jb);
        		buttons.put(jb, new Pair<>(i,j));
        	}
        } 
        this.setVisible(true);
    }

	private void draw() {
		for(JButton jb : buttons.keySet()) {
			String text=logic.getStatus(buttons.get(jb).getX(), buttons.get(jb).getY())?"*":"";
			jb.setText(text);
		}
		
		
	}
    
    
}
