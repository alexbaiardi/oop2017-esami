package a03b.e2;

import java.util.HashSet;
import java.util.Set;

public class LogicImpl implements Logic {

	private final Set<Pair<Integer,Integer>> hitted;
	private int size;
	
	
	public LogicImpl(int size) {
		hitted=new HashSet<>();
		this.size = size;
	}

	@Override
	public void hit(int x, int y) {
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				if((i+j)==(x+y)) {
					Pair<Integer,Integer> coord=new Pair<>(i, j);
					if(hitted.contains(coord)) {
						hitted.remove(coord);
					}else{
						hitted.add(coord);							
					}
				}
			}
		}

	}

	@Override
	public boolean getStatus(int x, int y) {
		return hitted.contains(new Pair<>(x, y));
	}

}
