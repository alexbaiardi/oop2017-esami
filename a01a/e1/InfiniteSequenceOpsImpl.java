package a01a.e1;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InfiniteSequenceOpsImpl implements InfiniteSequenceOps {

	@Override
	public <X> InfiniteSequence<X> ofValue(X x) {
		return () -> x;
	}

	@Override
	public <X> InfiniteSequence<X> ofValues(List<X> l) {
		Iterator<X> it=Stream.generate(l::stream).flatMap(e->e).iterator();
		return ()->it.next();
	}

	@Override
	public InfiniteSequence<Double> averageOnInterval(InfiniteSequence<Double> iseq, int intervalSize) {
		return ()->Stream.generate(iseq::nextElement).limit(intervalSize).mapToDouble(d->d).average().getAsDouble();
	}

	@Override
	public <X> InfiniteSequence<X> oneEachInterval(InfiniteSequence<X> iseq, int intervalSize) {
		return ()->Stream.generate(iseq::nextElement).skip(intervalSize-1).findFirst().get();
	}

	@Override
	public <X> InfiniteSequence<Boolean> equalsTwoByTwo(InfiniteSequence<X> iseq) {
		return ()->iseq.nextElement().equals(iseq.nextElement());
	}

	@Override
	public <X, Y extends X> InfiniteSequence<Boolean> equalsOnEachElement(InfiniteSequence<X> isx,
			InfiniteSequence<Y> isy) {
		return ()->isy.nextElement().equals(isx.nextElement());
	}

	@Override
	public <X> Iterator<X> toIterator(InfiniteSequence<X> iseq) {
		return Stream.generate(iseq::nextElement).iterator();
	}

}
