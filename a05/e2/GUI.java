package a05.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;



public class GUI extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private final Logic logic;
	private final List<JButton> buttonsA;
	private final List<JButton> buttonsB;
	
	public GUI(int size){
		
		logic=new LogicImpl(size, size);
		buttonsA=new LinkedList<>();
		buttonsB=new LinkedList<>();
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout());
		JPanel jNorth = new JPanel();
		JPanel jSouth = new JPanel();
		JPanel jCenter = new JPanel();
		this.getContentPane().add(BorderLayout.NORTH, jNorth);
		this.getContentPane().add(BorderLayout.SOUTH, jSouth);
		this.getContentPane().add(BorderLayout.CENTER, jCenter);
		for(int i=0;i<size;i++) {
			JButton bA=new JButton("*");
			JButton bB=new JButton("*");
			bA.setEnabled(false);
			bB.setEnabled(false);
			jNorth.add(bA);
			jSouth.add(bB);
			buttonsA.add(bA);
			buttonsB.add(bB);			
		}
		JButton play = new JButton("Play");
		jCenter.add(play);
		play.addActionListener(ac->{
			logic.hit();
			System.out.println("A="+logic.getResA()+" B="+logic.getResB());
			if(logic.isOver()) {
				System.exit(0);
			}
			draw(buttonsA,logic.getPointsA());
			draw(buttonsB,logic.getPointsB());
		});
		this.pack();
		this.setVisible(true);
	}

	private void draw(List<JButton> buttons, int points) {
		for(JButton jb : buttons) {
			String text=buttons.indexOf(jb)<points?"*":" ";
			jb.setText(text);
		}
		
	}

}

