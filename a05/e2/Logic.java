package a05.e2;

public interface Logic {
	
	void hit();
	
	int getResA();
	
	int getResB();
	
	boolean isOver();
	
	int getPointsA();
	
	int getPointsB();

}
