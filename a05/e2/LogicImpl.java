package a05.e2;

import java.util.Random;

public class LogicImpl implements Logic {

	private int pointsA;
	private int pointsB;
	private int resA;
	private int resB;
	
	
	public LogicImpl(int pointsA, int pointsB) {
		this.pointsA = pointsA;
		this.pointsB = pointsB;
	}

	@Override
	public void hit() {
		Random r=new Random();
		resA=r.nextInt(6)+1;
		resB=r.nextInt(6)+1;
		if(resA>resB) {
			pointsB--;
		} else {
			if(resB==resA) {
				pointsB--;
			}
			pointsA--;
		}
	}

	@Override
	public int getResA() {
		return resA;
	}

	@Override
	public int getResB() {
		return resB;
	}

	@Override
	public boolean isOver() {
		return pointsA==0||pointsB==0;
	}

	@Override
	public int getPointsA() {
		return pointsA;
	}

	@Override
	public int getPointsB() {
		return pointsB;
	}

}
