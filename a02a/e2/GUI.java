package a02a.e2;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
	private final Map<JButton, Pair<Integer,Integer>> buttons=new HashMap<>(); 
	private final Logic logic;
	
    public GUI(int size) {
    	logic=new LogicImpl(size);
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(size*100,size*100);
        JPanel panel = new JPanel(new GridLayout(size,size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            String text;
            text=logic.isHitted(buttons.get(bt).getX(), buttons.get(bt).getY())?"*":"";
            bt.setText(text);
            if(logic.isOver()) {
            	System.exit(0);
            }
        };
        for (int i=0;i<size;i++){
        	for(int j=0;j<size;j++) {
	            final JButton jb = new JButton();
	            jb.addActionListener(al);
	            panel.add(jb);
	            buttons.put(jb, new Pair<>(i, j));
        	}
        } 
        this.setVisible(true);
    }
    
}
