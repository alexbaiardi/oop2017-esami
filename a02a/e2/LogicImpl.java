package a02a.e2;

import java.util.HashSet;
import java.util.Set;


public class LogicImpl implements Logic {

	
	private Set<Pair<Integer,Integer>> hittedButtons=new HashSet<>();
	int size;
	
	public LogicImpl(int size) {
		this.size = size;
	}

	@Override
	public boolean isHitted(int x, int y) {
		Pair<Integer,Integer>coord=new Pair<>(x, y);
		return hittedButtons.contains(coord)?!hittedButtons.remove(coord):hittedButtons.add(coord);
	}

	@Override
	public boolean isOver() {
		int contR=0,contC=0;
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				if(hittedButtons.contains(new Pair<>(i, j))){
					contR++;
				}
				if(hittedButtons.contains(new Pair<>(j, i))){
					contC++;
				}
			}
			if(contR==size||contC==size) {
				return true;
			}
			contR=0;
			contC=0;
		}
		return false;
	}

}
