package a02a.e1;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SequenceAcceptorImpl implements SequenceAcceptor {

	private final Map<Sequence,Iterable<Integer>> sequences;
	private Pair<Sequence, Iterator<Integer>> currentSequence;

	public SequenceAcceptorImpl() {
		sequences = new HashMap<>();
		sequences.put(Sequence.POWER2, ()->IntStream.iterate(1, i -> i * 2).iterator());
		sequences.put(Sequence.FLIP, ()->IntStream.iterate(1, i -> (i + 1) % 2).iterator());
		sequences.put(Sequence.RAMBLE,()-> IntStream.iterate(1, i -> i + 1).map(e -> e % 2 == 0 ? e / 2 : 0).iterator());
		sequences.put(Sequence.FIBONACCI,()->
				Stream.iterate(new Pair<>(1, 1), i -> new Pair<>(i.getY(), i.getX() + i.getY())).map(p -> p.getX())
						.iterator());
	}

	@Override
	public void reset(Sequence sequence) {
		currentSequence = new Pair<>(sequence, sequences.get(sequence).iterator());
	}

	@Override
	public void reset() {
		check(currentSequence!=null);
		reset(currentSequence.getX());
	}

	@Override
	public void acceptElement(int i) {
		check(currentSequence!=null);
		check(currentSequence.getY().next()==i);

	}

	private void check(boolean condition) {
		if(!condition) {
			throw new IllegalStateException();
		}
	}

}
